#include<iostream>
#include<vector>
using namespace std;

int InitialGrid[9][9] = {
	{ 8,5,0,9,0,3,0,1,0 },
	{ 0,9,0,0,0,7,0,0,2 },
	{ 2,3,0,0,4,0,0,0,0 },
	{ 0,0,0,0,9,0,0,0,0 },
	{ 0,6,1,0,0,0,9,5,0 },
	{ 0,0,0,0,6,0,0,0,0 },
	{ 0,0,0,0,1,0,0,3,5 },
	{ 4,0,0,7,0,0,0,6,0 },
	{ 0,7,0,8,0,6,0,9,1 },
};
int* PtrGrid[81];

int grid[9][9];

int PosValGrid[9][9];

int c;
int d;
bool JePoleSJednouMoznosti = false;
int PtrPosition;

bool CheckGrid();
void FillGrid();
void RefillGrid();
int PrintGrid();
int PrintPosValGrid();
void FillPtrGrid();
int VratI(int);

bool CheckRaw(int i, int j, int n); //return true if the value n is in the raw
bool CheckColumn(int i, int j, int n); //return true if the value n is in the column
bool CheckSquare(int i, int j, int n); //return true if the value n is in the square
int VratiC(int i);
int VratiD(int j);

bool GoThrough(int PtrPosition);



bool CheckGrid()
{
	bool Finished = false;
	for (int i = 0; i < 9; i++)
	{
		for (int j = 0; j < 9; j++)
		{
			if (grid[i][j] == 0)
			{
				Finished = true;
				goto konecCheckGrid;
			}
		}
	}
konecCheckGrid:
	return Finished;
}



void FillGrid()
{
	for (int i = 0; i < 9; i++)
	{
		for (int j = 0; j < 9; j++)
		{
			grid[i][j] = InitialGrid[i][j];
		}
	}
}

void RefillGrid()
{
	for (int i = 0; i < 9; i++)
	{
		for (int j = 0; j < 9; j++)
		{
			if (!PosValGrid[i][j] == 0)
				grid[i][j] = PosValGrid[i][j];
		}
	}
}

int PrintGrid()
{
	for (int i = 0; i < 9; i++)
	{
		for (int j = 0; j < 9; j++)
		{
			cout << grid[i][j];
		}
		cout << "\n";
	}
	return 0;
}

int PrintPosValGrid()
{
	for (int i = 0; i < 9; i++)
	{
		for (int j = 0; j < 9; j++)
		{
			cout << PosValGrid[i][j];
		}
		cout << "\n";
	}
	return 0;
}

void FillPtrGrid()
{
	for (int n = 0; n < 81; n++)
	{
		PtrGrid[n] = &grid[n / 9][n % 9];
	}

}

int VratI(int PtrPosition)
{
	int i = PtrPosition / 9;
	//cout << "i je:" << i << endl;
		return i;
}

int VratJ(int PtrPosition)
{
	int j = PtrPosition%9;
	//cout << "j je:" << j << endl;
	return j;
}

int MovePlus(int PtrPosition)
{
	for (++PtrPosition; PtrPosition <= 80; PtrPosition++)
	{
		if (*PtrGrid[PtrPosition] == 0)
			break;
	}
		
	return PtrPosition;
}

void Print1DArry()
{
	for (int n = 0; n < 81; n++)
		cout << PtrGrid[n] << "\t"<<*PtrGrid[n]<<endl;
}

int VratiC(int i)
{
	if (i >= 0 && i < 3)
		c = 0;
	else
	{
		if (i >= 3 && i < 6)
			c = 3;
		else
			c = 6;
	}
	return c;
}

int VratiD(int j)
{
	if (j >= 0 && j < 3)
		d = 0;
	else
	{
		if (j >= 3 && j < 6)
			d = 3;
		else
			d = 6;
	}
	return d;
}


bool CheckRaw(int i, int j, int n)
{
	int b;
	bool jeTam;
	for (b = 0; b < 9; b++)
	{

		if (b == j)
			continue;

		if (grid[i][b] == n)
		{
			//cout << n << "R je na pozici" << i<<b<<"\n";
			jeTam = true;
			break;
		}

		else
		{
			//cout << n << "R tam neni" << "\n";
			jeTam = false;
		}

		//cout << n << "R neni tam" << "\n";

	}
	return jeTam;
}


bool CheckColumn(int i, int j, int n)
{
	int b;
	bool jeTam;
	for (b = 0; b < 9; b++)
	{
		if (b == i)
			continue;
		{
			if (grid[b][j] == n)
			{
				//cout << n << "C je tam" << "\n";
				jeTam = true;
				break;
			}
			else
			{
				jeTam = false;
			}
			//cout << n << "C neni tam" << "\n";
		}
	}
	return jeTam;
}


bool CheckSquare(int i, int j, int n)
{

	//int k;
	//int l;
	bool jeTam;

	//cout << "position" << i << j << "is checked. The value is" << grid[i][j] << "\n";
	for (int k = VratiC(i); k < (VratiC(i) + 3); k++)
	{


		for (int l = VratiD(j); l < (VratiD(j) + 3); l++)
		{

			if ((k == i) && (l == j))
				continue;


			if (grid[k][l] == n)
			{
				//cout << n << "je tam\n";
				jeTam = true;
				goto konec;

			}

			else
				jeTam = false;
			//cout << grid[k][l] << "\n";
		}


	}
konec:
	//cout << "konec";
	return jeTam;

}

bool GoThrough(int PtrPosition)
{
	bool FinishRekursion = true; //another nested GoThrough function starts
	{
		PtrPosition = MovePlus(PtrPosition);
		for (int value = 1; value <= 10; value++)
		{
			
			if (PtrPosition > 80)
			{
				FinishRekursion = false; //Rekursion over
				break;
			}
			{
				if (value == 10)
				{
					*PtrGrid[PtrPosition] = 0;
					break;
				}

				*PtrGrid[PtrPosition] = value;
				if (!(CheckRaw(VratI(PtrPosition), VratJ(PtrPosition), value) ||
					CheckColumn(VratI(PtrPosition), VratJ(PtrPosition), value) ||
					CheckSquare(VratI(PtrPosition), VratJ(PtrPosition), value)))
				{
					if (!GoThrough(PtrPosition))
					{
						FinishRekursion = false; // gradually emerge
						break;
					}
					
				}
			}

		}
	}
	return FinishRekursion;
}

int main()
{
	PtrPosition = (-1);
	FillGrid();
	FillPtrGrid();
	//Print1DArry();
	//cout << "i je:" << VratI(PtrPosition) << endl;
	//cout << "j je:" << VratJ(PtrPosition) << endl;
	//cout << grid[VratI(PtrPosition)][VratJ(PtrPosition)]<<endl;
	//cout << *PtrGrid[PtrPosition];
	//*PtrGrid[11] = 50;
	//cout << "i je:" << VratI(PtrPosition) << endl;
	//cout << "j je:" << VratJ(PtrPosition) << endl;
	//cout << grid[VratI(PtrPosition)][VratJ(PtrPosition)] << endl;
	//cout << *PtrGrid[PtrPosition];
	//cout<<MovePlus(0);
	//cout << PtrGrid[0];
	GoThrough(PtrPosition);
	PrintGrid();

	cin.get();
	return 0;
}